/**
* Name: Livraison
* Author: Cedric Buron
* Description: 
* Tags: Tag1, Tag2, TagN
*/

model Livraison

global{
	int nb_biens_livres<-0;
	int proba_creation<-5;
	int nb_caracs<-6;
	int nb_planete<-5;
	int nb_vaisseau<-10;
	list<point> localisation_planetes;
	init{
		create gestionnaire_planete number:nb_planete;
		create vaisseau number:nb_vaisseau;
		list<pair<gestionnaire_planete,gestionnaire_planete>> pairs;
	}
}

species bien{
	list<int> caracs;
	init{
		loop i from: 0 to:nb_caracs-1{
			add rnd(10) to:caracs;
		}
	}
	aspect base{
		draw square(1) color:#green;
		draw(string(self)) color:#black;
	}
}

species gestionnaire_planete skills:[fipa]{
	map<bien,message> utilities;
	point localisation;
	list<pair<bien,gestionnaire_planete>> a_assigner;
	reflex generate_bien when: rnd(1,100)<=proba_creation{
		create bien number:1 with:[location::self.location] returns:b;
		gestionnaire_planete g <- any(gestionnaire_planete where(each!=self));
		write '(Time ' + time + '): ' + name + ' sends a cfp message to all participants';
		add b[0]::g to:a_assigner;
	}
	reflex call when: !empty(a_assigner){
		loop b over:a_assigner{
			list<int> caracs;
			loop i from:0 to:nb_caracs-1{
				add (b.key).caracs[i] to: caracs;
			}
			if(!empty(vaisseau where(each.location=self.location))){
				do start_conversation (to: vaisseau where(each.location=self.location), protocol: 'fipa-contract-net', performative: 'cfp', contents: [b.key.name,caracs,b.value.name]);
			}
		}
	}
	reflex assign when: !empty(proposes){
		list<message> props<-proposes;
		write props;
		loop p over: a_assigner{
			list<message> ps <- props where(each.contents[0] = p.key.name);
			loop prop over: ps{
				bien b<- (bien where(each.name=prop.contents[0]))[0];
				gestionnaire_planete g <- (gestionnaire_planete where(each.name=prop.contents[2]))[0];
				pair<bien,gestionnaire_planete> b_assigner <- b::g;
				if(b_assigner in a_assigner){
					if(utilities[b]=nil or float(prop.contents[1])>float((utilities[b]).contents[1])){
						utilities[b] <- prop;
					}
				}
			}
		}
		loop p over:utilities{
			do accept_proposal message: p contents: [p.contents[0],p.contents[2]];
			write("assigne: " + p.contents[0] + " a: " + p.sender);
			pair<bien,gestionnaire_planete> b <- (bien where(each.name=p.contents[0]))[0]::(gestionnaire_planete where(each.name=p.contents[2]))[0];
			remove b from:a_assigner;
		}
		loop p over:props{
			if(!(p in utilities.values)){
				do reject_proposal message: p contents: [p.contents[0],p.contents[2]];
			}
		}
		utilities<-[];
	}
	aspect base{
		draw circle(5) color:#blue;
	}
}

species vaisseau skills:[moving,fipa]{
	float vitesse;
	list<int> caracs;
	bien transporte;
	bool en_attente;
	gestionnaire_planete planete_courante;
	init{
		planete_courante<-nil;
		location<-gestionnaire_planete[rnd(nb_planete-1)].location;
		loop i from:0 to: nb_caracs-1{
			add rnd(10) to:caracs;
		}
		vitesse <-float(rnd(1,4));
		en_attente<-false;
	}
	float utilite_bien(bien b){
		int result<-0;
		loop i  from: 0 to: nb_caracs-1{
			result<-result+caracs[i]*b.caracs[i];
		}
		return (result/(10*nb_caracs));
	}
	float utilite(bien b, gestionnaire_planete gb){
		return utilite_bien(b)*0.5+0.5*distance_to(self,gb)/vitesse;
	}
		
//	les agents utilisent une stratégie gloutonne: ils ne regardent que les noeuds qui les rapprochent de leur destination et calculent jusqu'à 10 étapes, pas plus.
	reflex moving when:planete_courante!=nil{
		do goto target:planete_courante speed:vitesse;
		if(transporte!=nil){
			transporte.location<-self.location;
		}
		if(planete_courante.location = self.location){
			write string(transporte) + ' delivre a destination.';
			nb_biens_livres<-nb_biens_livres+1;
			planete_courante<-nil;
			ask transporte{
				do die;
			}
			en_attente<-false;
		}
	}
	reflex maj_itineraire when: !empty(accept_proposals){
		list<message> accepts <- accept_proposals;
		loop a over: accepts{
			transporte <- (bien where(each.name=a.contents[0]))[0];
			planete_courante <- (gestionnaire_planete where(each.name=a.contents[1]))[0];
		}
	}
	reflex receive_reject_proposals when: !empty(reject_proposals) {
		message r <- reject_proposals[0];
		write '(Time ' + time + '): ' + name + ' receives a reject_proposal message from ' + agent(r.sender).name + ' with content ' + r.contents;
		en_attente<-false;
	}
	reflex proposer when: !empty(cfps){
		list<message> calls <-cfps;
		pair<message,float> meilleur<-calls[0]::0.0;
		loop c over: calls{
			float ut<- utilite((bien where(each.name=c.contents[0]))[0],(gestionnaire_planete where(each.name=c.contents[2]))[0]);
			if(ut>meilleur.value){
				meilleur<-c::ut;
			}
		}
		if(!en_attente){
			do propose message: meilleur.key contents:[meilleur.key.contents[0], meilleur.value, meilleur.key.contents[2]];
			en_attente<-true;
		}
	}
	aspect base{
		draw triangle(2) color:#red;
//		draw string(int(self)) color:#green;
	}
}

experiment univers{
	parameter "Nombre de planetes" var: nb_planete;
	parameter "Nombre de vaisseaux" var: nb_vaisseau;
	parameter "Probabilite de produire un bien à chaque tour (en %)" var: proba_creation;
	output{
		display espace{
			species gestionnaire_planete aspect:base;
			species vaisseau aspect:base;
			species bien aspect:base;
		}
		display metriques{
			chart "Nombre de biens livrés" size:{0.5,0.5} position:{0,0}{
				data "Nombre de biens livres" value: nb_biens_livres;
			}
			chart "Vaisseaux inoccupés" size:{0.5,0.5} position:{0.5,0}{
				data "Nombre de vaisseaux inoccupes" value: length(vaisseau where(each.planete_courante=nil));
			}
			chart "biens immobilisés" size:{0.5,0.5} position:{0,0.5}{
				data "Nombre de biens non pris en charge" value: length(list(bien)) - length(vaisseau where(each.planete_courante=nil));
			}
		}
	}
}